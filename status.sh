#!/bin/bash
TAIL=" . . . . . . . . . . . . . . . . . . . ."
marg=''

function show_stat {
    MODULE=$2
    echo "$marg$MODULE ${TAIL:${#MODULE}} $3"
    marg="${marg}    "
    cd $MODULE
    git status -s -b --porcelain | while read sl; do
        [ "$(echo $sl | grep -v '##')$(echo $sl | grep '\[')" ] && echo "$marg| $sl"
    done
    git submodule status | while read line ; do show_stat $line ; done
    marg=${marg:4}
    cd ..
}

function show_summary {
    cd $2
    git submodule summary | while read line; do [ "$line" ] && echo "$marg$line" ; done
    marg="${marg}    "
    git submodule status  | while read line ; do show_summary $line ; done
    marg=${marg:4}
    cd ..
}

startargs="hash $(basename $(pwd)) $(git rev-parse --abbrev-ref HEAD)"
echo
cd ..
show_stat $startargs
echo
show_summary $startargs
