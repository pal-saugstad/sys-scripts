#!/usr/bin/python

import sys, os, re, subprocess

def path_from_line(dest, folder, slash_end):
    vals = dest.split(' ')
    path = vals[0]
    if slash_end:
        dest_sub = '' if len(vals) < 2 else os.path.join(vals[1], '')
        src_sub = os.path.join(folder, '') if len(vals) < 3 else os.path.join(folder, vals[2], '')
    else:
        dest_sub = '.' if len(vals) < 2 else os.path.join('.', vals[1])
        src_sub = os.path.join(folder, '') if len(vals) < 3 else os.path.join(folder, vals[2])
        if dest_sub == './.':
            dest_sub = '.'
    return path, dest_sub, src_sub

def code_src_path(folders, slash_end=False):
    for folder in folders:
        pointer_file = '_'+folder[2:]+'_dest'
        dest = 'code'
        if os.path.exists(pointer_file):
            with open(pointer_file) as f:
                content = f.readlines()
                content = [x.strip() for x in content]
                for dest in content:
                    yield path_from_line(dest, folder, slash_end)
        else:
            yield path_from_line(dest, folder, slash_end)

def main():
    place = 'local'
    clean = 0
    update = 1
    force_write = 0
    input_options = 0
    file_filter_in = None
    file_filter_ex = None

    for parg in sys.argv:
        if parg[:1] != '-':
            continue
        if parg[:2] == '-x':
            file_filter_ex = parg[2:]
            parg = 'x'
        if parg[:2] == '-i':
            file_filter_in = parg[2:]
            parg = 'x'
        while True:
            parg = parg[1:]
            if not len(parg):
                break
            input_options = 1
            if parg[:1] == 'h':
                print "\nrsyn.py: Update files at the host"
                print "  -h help"
                print "  -r Write to real area"
                print "  -t Write to test area"
                print "  -f Force write files"
                print "  -b Write files, then clean aliens"
                print "  -c Clean alien files and folders"
                print "  -i<pattern> Regex pattern used for including cleaning"
                print "  -x<pattern> Regex pattern used for excluding cleaning\n"
                raise SystemExit
            if parg[:1] == 'b':
                clean = 1
                update = 1
            if parg[:1] == 'c':
                clean = 1
                update = 0
            if parg[:1] == 'f':
                force_write = 1
            if parg[:1] == 'r':
                place = 'real'
            if parg[:1] == 't':
                place = 'test'
    if not input_options:
        clean = 1

    if place == 'real' and not clean:
        if not re.search('^(y|Y)$', raw_input("Update 'real' [y|N]? ")):
            raise SystemExit

    homedir = os.path.dirname(os.path.realpath(__file__))
    os.chdir(homedir)

    with open(os.path.join("config", place, "rsyn.ini")) as f:
        content = f.readlines()

    options = {}
    for x in content:
        y = x.strip()
        key, value = y.split(': ')
        options[key] = value

    for option in ('host', 'user', 'cpre', 'area', 'data'):
        if option not in options:
            print "Missing option", option, "in", ini_file
            raise SystemExit

    # print options

    code = os.path.join(options['cpre'],options['area'], '')
    REF = os.path.join('config', place, 'stamp')

    # print code, REF

    link_src = {'constants': os.path.join('config', place, 'constants'),
                'common': "config/common"}
    for d in link_src:
        if os.path.exists(d):
            os.unlink(d)
        os.symlink(link_src[d], d)

    MODULES = subprocess.check_output(["""find . -maxdepth 1 -mindepth 1 -type l; git submodule | awk '{print "./"$2}'"""],shell=True).splitlines()

    if update:
        do_update(MODULES, force_write, options['user']+'@'+options['host'], code, options['data'], os.path.join(homedir, REF))
    if clean:
        do_clean(MODULES, force_write, options['user']+'@'+options['host'], code, file_filter_in, file_filter_ex, place == 'real')

    for d in link_src:
        if os.path.exists(d):
            os.unlink(d)


def do_clean(MODULES, force_write, server, code, file_filter_in, file_filter_ex, real):
    print "Cleaning aliens"
    hash_src = {}

    # __a__
    # Make an overview of all source files in as a hash over where they are expected to be found at target
    for path, dest_sub, src_sub in code_src_path(MODULES):
        if path == 'code':
            cmd = 'cd {}; find . -type f ! -path "./.git*"'.format(src_sub)
            # print cmd
            lresp = subprocess.check_output([cmd], shell=True).splitlines()
            # print lresp
            for line in lresp:
                hash_src[os.path.join(dest_sub, line[2:])] = 1
    # print hash_src

    # __b__
    # fetch list of all files at target
    cmd="ssh {} 'cd {} && find -type f | grep -v test-area'".format(server, code)
    # print cmd
    resp = subprocess.check_output([cmd], shell=True).splitlines()
    # print resp

    # Compare __a__ and __b__ using include and exclude patterns
    exe = []
    for line in resp:
        if line not in hash_src:
            exclude = False
            include = True
            if file_filter_in:
                include = False
                if re.search(file_filter_in, line):
                    include = True
            if file_filter_ex:
                if re.search(file_filter_ex, line):
                    exclude = True
            if not exclude and include:
                exe.append(line)
    # print exe
    if not len(exe):
        print "No files to remove"
    while len(exe):
        deletes = '"'+'" "'.join(exe[:40])+'"'
        rcmd = "ssh {} 'cd {} && rm ".format(server, code)+deletes+"'"
        del exe[:40]
        print "DELETE FILES: cd", code, "&& rm\n", deletes
        do_delete = True
        if real:
            if not re.search('^(y|Y)$', raw_input("Delete these files [y|N]? ")):
                do_delete = False
        if do_delete:
            print "Deleted"
            subprocess.call([rcmd], shell=True)
        else:
            print "Skipping"

    # Remove empty directories
    cmd = "ssh {} 'cd {} && find . -depth -type d -empty -exec rmdir {{}} \\;'".format(server, code)
    subprocess.call([cmd], shell=True)

def do_update(MODULES, force_write, server, code, data, stamp):

    folders = []
    for MODULE in MODULES:

        cmd = 'cd {} && find . -newer {} ! -path "./.git*"'.format(MODULE, stamp)
        if force_write or re.search('/', subprocess.check_output([cmd], shell=True)):
            folders.append(MODULE)

    if len(folders):
        for path, dest_sub, src_sub in code_src_path(folders, True):
            print "From", src_sub, '({})'.format(path)
            if path == 'code':
                cmd = "rsync -uva --exclude=.git {} {}:{}{}".format(src_sub, server, code, dest_sub)
                # print cmd
                print subprocess.check_output([cmd], shell=True)
            elif path == 'data' and dest_sub:
                cmd = "rsync -uvr --delete --exclude=.git {} {}:{}{}".format(src_sub, server, data, dest_sub)
                # print cmd
                print subprocess.check_output([cmd], shell=True)
    else:
        print "No new files"
    subprocess.call(['touch {}'.format(stamp)], shell=True)

main()
