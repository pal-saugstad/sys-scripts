#!/bin/bash
git submodule init
git submodule foreach 'git pull'
git submodule foreach 'git checkout `git branch --contains $sha1 | grep -v "detached from" | sed s/^\*// | { read first rest ; echo $first; }`' >/dev/null
./status.sh
