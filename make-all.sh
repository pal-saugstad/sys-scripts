#!/bin/bash
cd $(dirname "$0")

dirs=$(find -follow -name make.sh)

for DIR in $dirs
do
    pushd $(dirname $DIR)
    ./make.sh
    popd
done
./rsyn.py
